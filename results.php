<?php
/**
 * Template Name: Results
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <div class="container-fluid wrap">
        <div class="row center-xs">
            <div class="col-xs-12 col-md-10">
                <form action="<?php the_permalink(); ?>" class="hotels__search-form">
                    <div class="row middle-xs start-xs">
                        <div class="col-xs-5">
                            <label for="location" class="font__secondary--sc text-color__main font-size__small--x letter-spacing__medium"><?php _e('where', 'gh-indie'); ?></label>
                            <select name="location" id="location"><?php _e('Select a city', 'gh-indie'); ?>
                                <option value=""><?php _e('Select a City', 'gh-indie'); ?></option>
                                <?php
                                    $rest_url = 'https://back.guruhotel.com/hotels';
                                    $results = file_get_contents($rest_url);
                                    $results = json_decode($results);
                                    $locations = array();
                                    foreach($results as $result){
                                        $location_id = $result->location->id;

                                       if(empty($locations[$location_id])){

                                        $locations[$location_id]->id = $location_id;
                                        $locations[$location_id]->city = $result->location->city;
                                        $locations[$location_id]->country = $result->location->country;
                                       }
                                   }
                                    foreach ($locations as $location):
                                ?>
                                    <option value=""><?php echo $location->city.', '.$location->country; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="col-xs-5">
                            <label for="litepicker" class="font__secondary--sc text-color__main font-size__small--x letter-spacing__medium"><?php _e('when', 'gh-indie'); ?></label>
                            <input name="dates" id="litepicker" type="text" placeholder="Check in - Check out" value="<?php echo $_GET['dates']; ?>">
                        </div>

                        <button type="submit"><ion-icon name="search-outline" class="text-color__secondary font-size__medium"></ion-icon></button>
                    </div>
               </form>
            </div>
        </div>
    </div>

    <?php
        $filter_location = $_GET['filter_location'] ? '&location.country=' . $_GET['filter_location'] : '';
        $offset = $_GET['offset'] ? '&_start=' . ($_GET['offset'] * 9) : '';
        $sortby = $_GET['sortby'] ? $_GET['sortby'] : 'recent';

        $rest_url = 'https://back.guruhotel.com/hotels?_limit=9&_sort=order&featured_hotel=1'.$offset.$filter_location;
        $results = file_get_contents($rest_url);
        $results = json_decode($results);
    ?>

    <section class="results__list background-color__secondary">
        <div class="container-fluid wrap">
            <div class="row center-xs middle-xs">
                <div class="col-xs-12 col-md-2 start-xs">
                    <span class="text-color__main font-weight__bold letter-spacing__normal">200</span> <?php _e('results', 'gh-indie'); ?>
                </div>

                <div class="col-xs-12 col-md-8 end-xs">
                    <div class="hotels-orderby__select-wrapper display__inline--block">
                        <select name="order" id="orderby">
                            <option value=""><?php _e('Order by Low Price', 'gh-indie'); ?></option>
                        </select>
                        <ion-icon name="chevron-down-outline"></ion-icon>
                    </div>

                </div>
            </div>

            <div class="row center-xs">

                <div class="col-xs-12 col-md-10 margin-top__mega--x">
                    <?php foreach($results as $result): ?>
                        <article class="hotel-card background-color__white border-radius__medium--x margin-bottom__big">
                            <div class="row start-xs middle-xs">
                                <div class="col-xs-12 col-md-5">
                                    <?php
                                        $imageID = $result->featured_image;
                                        if($imageID){
                                            $imageKey = array_search($imageID, array_column($result->images, 'id'));
                                            $imageUrl = $result->images[$imageKey]->formats->medium->url ? $result->images[$imageKey]->formats->medium->url : $result->images[$imageKey]->url;
                                            $imageUrl = $imageUrl ? $imageUrl : $result->photos[$imageKey]->formats->medium->url;
                                        }
                                        else{
                                            $imageUrl = $result->images[0]->formats->medium->url ? $result->images[0]->formats->medium->url : $result->images[0]->url;
                                            $imageUrl = $imageUrl ? $imageUrl : $result->photos[0]->formats->medium->url;
                                        }
                                    ?>
                                    <?php if (!empty($imageUrl)): ?>
                                        <a href="#" class="hotel__photo">
                                             <img src="<?php echo $imageUrl; ?>" alt="<?php echo $result->name; ?> Photo" loading="lazy">
                                        </a>
                                    <?php elseif(!empty($result->logo->url)): ?>
                                        <img src="<?php echo $result->logo->url; ?>" alt="<?php echo $result->name; ?> Logo" loading="lazy" class="hotel__logo">
                                    <?php endif ?>
                                </div>
                                <div class="col-xs-12 col-md-4 padding-left__mega--x hotel__info">
                                    <div class="hotel__location">
                                        <ion-icon name="navigate-circle-outline"></ion-icon>
                                        <span class="font-size__small--x"><?php echo $result->location->city.', '.$result->location->country; ?></span>
                                    </div>
                                    <div>
                                        <h3><?php echo $result->name; ?></h3>
                                    </div>
                                    <div class="hotel__stars text-color__main">
                                        <?php $stars = $result->stars; for($star = 1; $star<=$stars; $star++): ?>
                                            <ion-icon name="star"></ion-icon>
                                        <?php endfor; ?>
                                    </div>
                                    <div class="hotel__details">
                                        <span>10 rooms</span>
                                        <span>45m2</span>
                                        <span>up to 3 guests</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-3 padding-right__mega--x">
                                    <div class="hotel__price end-xs">
                                        <div>
                                            <h5 class="font-weight__bold"><?php _e('starting at', 'gh-indie'); ?></h5>
                                            <span class="hotel__price--amount font-size__big text-color__main font-weight__bold">$150</span>
                                            <h5 class="font-weight__bold margin-bottom__big"><?php _e('night', 'gh-indie'); ?></h5>
                                        </div>


                                        <?php
                                            if (!empty($result->subdomain)){
                                                $hotelLink = 'http://'.$result->subdomain;
                                            }
                                            else{
                                                $parsed = parse_url($result->website);
                                                if (empty($parsed['scheme'])) {
                                                    $hotelLink = 'http://' . ltrim($result->website, '/');
                                                }
                                            }
                                         ?>
                                        <?php if (!empty($result->subdomain)): ?>
                                            <a href="<?php echo $hotelLink; ?>" target="_blank" class="btn"><?php _e('Book now', 'gh-indie'); ?></a>
                                        <?php else: ?>
                                            <a href="<?php echo $hotelLink; ?>" target="_blank" class="btn"><?php _e('Book now', 'gh-indie'); ?></a>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </article>
                    <?php endforeach; ?>
                </div>

                <div class="col-xs-12 col-md-10">
                    <?php
                        //Count hotels
                        $offset = $_GET['offset'];
                        $count_url = 'https://back.guruhotel.com/hotels/count?_where[featured_hotel]=1';
                        $hotels_count = file_get_contents($count_url);
                        $posts_per_page = 9;

                        if($hotels_count>$posts_per_page):

                            $pages_count = ceil($hotels_count / $posts_per_page);

                            if ($pages_count <= 10) {
                                $start = 1;
                                $end   = $pages_count;
                            } else {
                                $start = max(1, ($offset - 4));
                                $end   = min($pages_count, ($offset + 5));

                                if ($start === 1) {
                                    $end = 10;
                                } elseif ($end === $pages_count) {
                                    $start = ($pages_count - 9);
                                }
                            }
                    ?>
                        <div class="hotels-pagination">
                            <ul>
                                <?php if (!empty($offset) && $offset > 0 ): ?>
                                    <li><a href="<?php the_permalink(); ?>?featured_hotel=1&offset=<?php echo $offset - 1; ?>"><ion-icon name="chevron-back-outline"></ion-icon></a></li>
                                <?php endif ?>

                                <?php for($page = $start; $page<=$end; $page++): ?>
                                    <li <?php if($offset + 1 == $page || ($page == 1 && empty($offset))) echo 'class="current"'; ?>><a href="<?php the_permalink(); ?>?featured_hotel=1&offset=<?php echo $page - 1; ?>"><?php echo $page; ?></a></li>
                                <?php endfor; ?>

                                <?php if ($offset + 1 < $pages_count ): ?>
                                    <li><a href="<?php the_permalink(); ?>?featured_hotel=1&offset=<?php echo $offset + 1; ?>"><ion-icon name="chevron-forward-outline"></ion-icon></a></li>
                                <?php endif ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; get_footer();
