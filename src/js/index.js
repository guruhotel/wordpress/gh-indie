//Imports
import slick from 'slick-carousel';
import AOS from 'aos';
import lity from 'lity';
import Litepicker from 'litepicker';

//Site code
jQuery(document).ready(function($){
    //Animations
    AOS.init();

    //Fade in/out animation
    window.addEventListener("beforeunload", function () {
      document.body.classList.add("animate-out");
    });

    $('.home__main-banner--slider').slick({
        rows: 0,
        arrows: true,
        dots: true,
        speed: 1000,
        fade: true,
        prevArrow: '<button type="button" class="slick-prev"><ion-icon name="arrow-back-outline"></ion-icon></button>',
        nextArrow: '<button type="button" class="slick-next"><ion-icon name="arrow-forward-outline"></ion-icon></button>',
        lazyLoad: "progressive",
        autoplay: true
    })

    $('.home__main-banner--slider .aos-animate').removeClass('aos-animate');
    $('.home__main-banner--slider [data-aos]').addClass('aos-animate');

    $('.home__main-banner--slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('.home__main-banner--slider .aos-animate').removeClass('aos-animate');
    });

    $('.home__main-banner--slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
        $('.home__main-banner--slider [data-aos]').addClass('aos-animate');
    });

    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 100 ) {
            jQuery('.primary-header').addClass('fixed');
        }
        else {
            jQuery('.primary-header').removeClass('fixed');
        }
    });

    var today = new Date();
    today.setHours(0, 0, 0, 0);

    var picker = new Litepicker({
        element: document.getElementById('litepicker'),
        format: 'MM/DD/YYYY',
        hotelMode: true,
        showTooltip: true,
        mobileFriendly: true,
        numberOfMonths: 2,
        numberOfColumns: 2,
        singleMode: false,
        minDate: today
    });
});


