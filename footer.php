    <footer class="primary-footer background-color__secondary">
        <div class="container-fluid wrap">
            <div class="row middle-xs">
                <div class="col-xs-6 col-sm-4">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-powered.svg">
                </div>
                <div class="col-xs-6 col-sm-4 center-xs">
                    <ul class="list-unstyled social-nav list-horizontal display__inline--block">
                        <?php if(get_field('facebook_url', 'option')): ?>
                            <li>
                                <a href="<?php the_field('facebook_url', 'option'); ?>" target="_blank">
                                    <ion-icon name="logo-facebook"></ion-icon>
                                </a>
                            </li>
                       <?php endif; if(get_field('instagram_url', 'option')): ?>
                           <li>
                               <a href="<?php the_field('instagram_url', 'option'); ?>" target="_blank">
                                   <ion-icon name="logo-instagram"></ion-icon>
                               </a>
                           </li>
                       <?php endif; if(get_field('twitter_url', 'option')): ?>
                            <li>
                                <a href="<?php the_field('twitter_url', 'option'); ?>" target="_blank">
                                    <ion-icon name="logo-twitter"></ion-icon>
                                </a>
                            </li>
                       <?php endif; if(get_field('linkedin_url', 'option')): ?>
                            <li>
                                <a href="<?php the_field('linkedin_url', 'option'); ?>" target="_blank">
                                    <ion-icon name="logo-linkedin"></ion-icon>
                                </a>
                            </li>
                       <?php endif; if(get_field('spotify_url', 'option')): ?>
                            <li>
                                <a href="<?php the_field('spotify_url', 'option'); ?>" target="_blank">
                                    <ion-icon name="logo-spotify"></ion-icon>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-xs-11 col-sm-4 end-xs hidden__xs">
                    <nav class="main-menu">
                        <?php wp_nav_menu( array( 'theme_location' => 'menu-main', 'container' => '' ) ); ?>
                    </nav>
                </div>
            </div>
        </div>
    </footer>

<?php wp_footer(); ?>
</body>
</html>
