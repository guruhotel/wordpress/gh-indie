<?php
/**
 * Template Name: Home
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

     <section id="home__main-banner">

        <div class="hotels__search-form--wrapper">
            <div class="container-fluid wrap">
                <form action="<?php the_permalink(115); ?>" class="background-color__secondary hotels__search-form">

                <div class="row middle-xs">
                    <div class="col-xs-5">
                        <label for="location" class="font__secondary--sc text-color__main font-size__small--x letter-spacing__medium"><?php _e('where', 'gh-indie'); ?></label>
                        <select name="location" id="location"><?php _e('Select a city', 'gh-indie'); ?>
                            <option value=""><?php _e('Select a City', 'gh-indie'); ?></option>
                            <?php
                                $rest_url = 'https://back.guruhotel.com/hotels';
                                $results = file_get_contents($rest_url);
                                $results = json_decode($results);
                                $locations = array();
                                foreach($results as $result){
                                    $location_id = $result->location->id;

                                   if(empty($locations[$location_id])){

                                    $locations[$location_id]->id = $location_id;
                                    $locations[$location_id]->city = $result->location->city;
                                    $locations[$location_id]->country = $result->location->country;
                                   }
                               }
                                foreach ($locations as $location):
                            ?>
                                <option value=""><?php echo $location->city.', '.$location->country; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-xs-5">
                        <label for="litepicker" class="font__secondary--sc text-color__main font-size__small--x letter-spacing__medium"><?php _e('when', 'gh-indie'); ?></label>
                        <input name="dates" id="litepicker" type="text" placeholder="Check in - Check out">
                    </div>

                    <button type="submit"><ion-icon name="search-outline" class="text-color__secondary font-size__big"></ion-icon></button>
                </div>
                </form>
            </div>
        </div>


        <div class="home__main-banner--slider">
            <?php if(have_rows('home_slider')) : while(have_rows('home_slider')): the_row(); ?>
            <article class="item">
                <div class="banner__image">
                    <img src="<?php $img = get_sub_field('image'); echo $img['sizes']['large']; ?>">
                </div>

                <div class="banner__location">
                    <h4 class="font__secondary--sc text-color__secondary font-size__small--x letter-spacing__medium"><?php the_sub_field('photo_footer'); ?></h4>
                </div>

                <div class="container-fluid wrap">
                    <div class="row">
                        <div class="col-xs-12 col-md-5 col-lg-6 col-md-offset-1 col-lg-offset-0">
                            <?php if(get_sub_field('title')): ?>
                               <h1 class="banner__title text-color__black" data-aos="fade-up"><?php the_sub_field('title'); ?></h1>
                            <?php endif; ?>

                             <?php if (get_sub_field('subtitle')): ?>
                                <h2 class="banner__subtitle font-weight__normal text-color__main" data-aos="fade-up" data-aos-delay="200"><?php the_sub_field('subtitle'); ?></h2>
                            <?php endif ?>

                            <?php if(get_sub_field('text')): ?>
                               <p class="banner-desc font-size__medium text-color__titles" data-aos="fade-up" data-aos-delay="400"><?php the_sub_field('text'); ?></p>
                           <?php endif; ?>

                           <div class="banner__footer font-size__small--x">
                               <ion-icon name="globe-outline"></ion-icon> Don’t know where to go?  <a href="#featured-locations">See our suggestions</a>
                           </div>
                        </div>
                    </div>
                </div>
            </article>
        <?php endwhile; endif; ?>
        </div>

        <div class="powered-bar background-color__black">
            <a href="http://guruhotel.com" target="_blank">
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/powered.svg" alt="Powered by GuruHotel">
            </a>
        </div>
    </section>

    <?php if(have_rows('home_locations')) : ?>
        <section class="home__locations padding__small-section">
            <div class="container-fluid wrap">
                <div class="col-xs-12 col-md-6 col-lg-5">
                    <h2 data-aos="fade-up"><?php the_field('home_locations_title'); ?></h2>
                    <div data-aos="fade-up"><?php the_field('home_locations_text'); ?></div>
                </div>

                <div class="home__locations--list">
                    <div class="row">
                        <?php while(have_rows('home_locations')): the_row(); ?>
                        <article class="home__location col-xs-12 col-sm-6 col-md-3 margin-bottom__medium" data-aos="fade-up" data-aos-delay="200">
                            <div class="home__location-wrap">
                                <a href="">
                                    <img src="<?php $img = get_sub_field('image'); echo $img['sizes']['medium_large']; ?>">
                                </a>

                                <div class="text-wrapper center-xs">
                                    <h4 class="text-color__secondary"><?php the_sub_field('title'); ?></h4>
                                    <h5 class="text-color__secondary">15 hotels</h5>
                                </div>
                            </div>
                        </article>
                    <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if(have_rows('home_features')) : ?>
        <section class="home__features padding__small-section">
            <div class="container-fluid wrap">
                <div class="col-xs-12 col-md-6 col-lg-5">
                    <h2 data-aos="fade-up"><?php the_field('home_features_title'); ?></h2>
                    <div data-aos="fade-up"><?php the_field('home_features_text'); ?></div>
                </div>

                <div class="home__features--list">
                    <div class="row">
                        <?php while(have_rows('home_features')): the_row(); ?>
                        <article class="home__feature col-xs-12 col-sm-4 col-md-3 col-md-offset-1 margin-bottom__mega" data-aos="fade-up" data-aos-delay="200">
                            <div class="feature-icon background-color__main margin-bottom__big">
                                <img src="<?php $img = get_sub_field('icon'); echo $img['sizes']['medium']; ?>">
                            </div>
                            <h3><?php the_sub_field('title'); ?></h3>
                            <p><?php the_sub_field('text'); ?></p>
                        </article>
                    <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

<?php endwhile; get_footer();
