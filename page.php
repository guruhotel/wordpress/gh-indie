<?php
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<section>
    <div class="background-color__titles">
        <div class="alpha-bg-inverse padding-top__section padding-bottom__small-section page-default-header">
            <div class="container-fluid wrap">
                <div class="row center-xs align-center">
                    <div class="col-xs-12 col-md-9">
                        <h1 class="font-size__big--x text-color__white"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid wrap padding-top__small-section padding-bottom__small-section">
        <div class="row center-xs">
            <div class="col-xs-12 col-md-9 start-xs">
                <div class="page-content border-color__grey--regent card background-color__white border-radius__medium card__size--mega box-shadow__small">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endwhile; wp_reset_query(); // End of the loop. ?>

<?php
get_footer();
