<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link href="https://fonts.googleapis.com/css2?family=Karla:wght@400;700&family=Vollkorn+SC&family=Vollkorn:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">
	<script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule="" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <?php wp_head(); ?>
</head>

<body <?php body_class('animate-in'); ?>>
    <header class="primary-header">
        <div class="container-fluid wrap">
            <div class="row middle-xs">
                <div class="col-xs-6">
                    <a href="<?php bloginfo('wpurl'); ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.svg">
                    </a>
                </div>
                <div class="col-xs-6">
                    <nav class="main-menu">
                        <?php wp_nav_menu( array( 'theme_location' => 'menu-main', 'container' => '' ) ); ?>
                    </nav>
                </div>
            </div>
        </div>
    </header>
